[vars]
machine_api_key = "machine-api-key"
provider_domain = "your.revad.com"
shared_secret = "shared-secret-1"

[log]
level = "trace"

[shared]
gatewaysvc = "your.revad.com:19000"

[grpc.services.gateway]
address = "your.revad.com:19000"
authregistrysvc = "{{ grpc.services.authregistry.address }}"
appregistrysvc = "{{ grpc.services.appregistry.address }}"
storageregistrysvc = "{{ grpc.services.storageregistry.address }}"
preferencessvc = "{{ grpc.services.userprovider.address }}"
userprovidersvc = "{{ grpc.services.userprovider.address }}"
usershareprovidersvc = "{{ grpc.services.usershareprovider.address }}"
ocmcoresvc = "{{ grpc.services.ocmcore.address }}"
ocmshareprovidersvc = "{{ grpc.services.ocmshareprovider.address }}"
ocminvitemanagersvc = "{{ grpc.services.ocminvitemanager.address }}"
ocmproviderauthorizersvc = "{{ grpc.services.ocmproviderauthorizer.address }}"
datagateway = "https://{{ http.services.datagateway.address }}/data"

transfer_expires = 6     # give it a moment
commit_share_to_storage_grant = true
commit_share_to_storage_ref = true


[grpc.services.appregistry]
driver = "static"

[grpc.services.appregistry.drivers.static]
mime_types = [
    { "mime_type" = "text/plain", "extension" = "txt", "name" = "Text file", "description" = "Text file", "allow_creation" = true },
    { "mime_type" = "text/markdown", "extension" = "md", "name" = "Markdown file", "description" = "Markdown file", "allow_creation" = true },
    { "mime_type" = "application/vnd.oasis.opendocument.text", "extension" = "odt", "name" = "OpenDocument", "description" = "OpenDocument text document", "default_app" = "Collabora", "allow_creation" = true },
    { "mime_type" = "application/vnd.oasis.opendocument.spreadsheet", "extension" = "ods", "name" = "OpenSpreadsheet", "description" = "OpenDocument spreadsheet document", "default_app" = "Collabora", "allow_creation" = true },
    { "mime_type" = "application/vnd.oasis.opendocument.presentation", "extension" = "odp", "name" = "OpenPresentation", "description" = "OpenDocument presentation document", "default_app" = "Collabora", "allow_creation" = true },
    { "mime_type" = "application/vnd.jupyter", "extension" = "ipynb", "name" = "Jupyter Notebook", "description" = "Jupyter Notebook" }
]


### AUTH PROVIDERS ###

[grpc.services.authregistry]
driver = "static"

[grpc.services.authregistry.drivers.static.rules]
basic = "{{ grpc.services.authprovider[0].address }}"
machine = "{{ grpc.services.authprovider[1].address }}"
ocmshares = "{{ grpc.services.authprovider[2].address }}"

[[grpc.services.authprovider]]
auth_manager = "nextcloud"

[grpc.services.authprovider.auth_managers.nextcloud]
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "{{ vars.shared_secret }}"
mock_http = false

[[grpc.services.authprovider]]
auth_manager = "machine"

[grpc.services.authprovider.auth_managers.machine]
api_key = "{{ vars.machine_api_key }}"
gateway_addr = "your.revad.com:19000"

[[grpc.services.authprovider]]
auth_manager = "ocmshares"


### STORAGE PROVIDERS ###

[grpc.services.storageregistry]
driver = "static"

[grpc.services.storageregistry.drivers.static]
home_provider = "/home"

[grpc.services.storageregistry.drivers.static.rules]
"/home" = { "address" = "{{ grpc.services.storageprovider[0].address }}" }
"nextcloud" = { "address" = "{{ grpc.services.storageprovider[0].address }}" }
"/ocm" = { "address" = "{{ grpc.services.storageprovider[1].address }}" }
"ocm" = { "address" = "{{ grpc.services.storageprovider[1].address }}" }

[[grpc.services.storageprovider]]
driver = "nextcloud"
mount_id = "nextcloud"
expose_data_server = true
enable_home_creation = false
data_server_url = "https://your.revad.com:{{ http.services.dataprovider[0].address.port }}/data"

[grpc.services.storageprovider.drivers.nextcloud]
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "{{ vars.shared_secret }}"
mock_http = false

[[grpc.services.storageprovider]]
driver = "ocmoutcoming"
mount_id = "ocm"
mount_path = "/ocm"
expose_data_server = true
enable_home_creation = false
data_server_url = "https://your.revad.com:{{ http.services.dataprovider[1].address.port }}/data"

[grpc.services.storageprovider.drivers.ocmoutcoming]
machine_secret = "{{ vars.machine_api_key }}"


### OTHER PROVIDERS ###

[grpc.services.usershareprovider]
driver = "memory"

[grpc.services.ocmcore]
driver = "nextcloud"

[grpc.services.ocmcore.drivers.nextcloud]
host = "https://your.revad.com/"
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "{{ vars.shared_secret }}"
mock_http = false

[grpc.services.ocminvitemanager]
# TODO the driver should become "nextcloud"
driver = "json"
provider_domain = "your.revad.com"

[grpc.services.ocmshareprovider]
driver = "nextcloud"
provider_domain = "your.revad.com"
endpoint = "https://your.revad.com/"
webdav_endpoint = "https://your.revad.com/"
webdav_prefix = "https://your.revad.com/remote.php/dav/files"
webapp_template = "https://your.revad.com/external/sciencemesh/{{.Token}}/{relative-path-to-shared-resource}"

[grpc.services.ocmshareprovider.drivers.nextcloud]
webdav_host = "https://your.revad.com/"
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "{{ vars.shared_secret }}"
mock_http = false
mount_id = "nextcloud"

[grpc.services.ocmproviderauthorizer]
driver = "json"

[grpc.services.ocmproviderauthorizer.drivers.json]
providers = "providers.testnet.json"
verify_request_hostname = true

[grpc.services.userprovider]
driver = "nextcloud"

[grpc.services.userprovider.drivers.nextcloud]
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "shared-secret-1"
mock_http = false

[grpc.services.datatx]
txdriver = "rclone"
storagedriver = "json"
remove_transfer_on_cancel = true

[grpc.services.datatx.txdrivers.rclone]
# rclone endpoint
endpoint = "http://rclone.docker"
# basic auth is used
auth_user = "rcloneuser"
auth_pass = "eilohtho9oTahsuongeeTh7reedahPo1Ohwi3aek"
auth_header = "x-access-token"
job_status_check_interval = 2000
job_timeout = 120000
storagedriver = "json"
remove_transfer_job_on_cancel = true

[grpc.services.datatx.storagedrivers.json]
file = ""

[grpc.services.datatx.txdrivers.rclone.storagedrivers.json]
file = ""


### HTTP ENDPOINTS ###

[http]
certfile = "/etc/revad/tls/your.revad.ssl.crt"
keyfile = "/etc/revad/tls/your.revad.ssl.key"

[http.services.appprovider]
address = "0.0.0.0:443"
insecure = true

[http.services.datagateway]
address = "0.0.0.0:443"

[[http.services.dataprovider]]
driver = "nextcloud"

[http.services.dataprovider.drivers.nextcloud]
endpoint = "https://your.efss.com/index.php/apps/sciencemesh/"
shared_secret = "shared-secret-1"
mock_http = false

[[http.services.dataprovider]]
driver = "ocmoutcoming"

[http.services.dataprovider.drivers.ocmoutcoming]
machine_secret = "{{ vars.machine_api_key }}"

[http.services.sciencemesh]
address = "0.0.0.0:443"
provider_domain = "your.revad.com"
mesh_directory_url = "https://meshdir.docker/meshdir"
# for a production deployment, use:
# mesh_directory_url = 'https://sciencemesh.cesnet.cz/iop/meshdir'
ocm_mount_point = "/sciencemesh"

[http.services.sciencemesh.smtp_credentials]
disable_auth = true
sender_mail = "sciencemesh@your.revad.com"
smtp_server = "smtp.your.revad.com"
smtp_port = 25

[http.services.ocmprovider]
address = "0.0.0.0:443"
ocm_prefix = "ocm"
provider = "Reva for ownCloud/Nextcloud"
endpoint = "https://your.revad.com"
enable_webapp = true
enable_datatx = true

[http.services.ocmd]
address = "0.0.0.0:443"
prefix = "ocm"

[http.services.ocmd.config]
host = "your.revad.com"

[http.services.ocs]
address = "0.0.0.0:443"
prefix = "ocs"

[http.services.ocdav]
address = "0.0.0.0:443"

[http.services.prometheus]
[http.services.sysinfo]

[http.middlewares.cors]
[http.middlewares.log]
